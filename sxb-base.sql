/*
Navicat MySQL Data Transfer

Source Server         : biubiu
Source Server Version : 50650
Source Host           : localhost:3306
Source Database       : sxb-base

Target Server Type    : MYSQL
Target Server Version : 50650
File Encoding         : 65001

Date: 2022-05-20 11:51:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_e
-- ----------------------------
DROP TABLE IF EXISTS `sys_e`;
CREATE TABLE `sys_e` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `des` varchar(510) DEFAULT NULL,
  `tag` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `sname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_e
-- ----------------------------
INSERT INTO `sys_e` VALUES (''101'', ''四川长虹电器股份有限公司'', ''四川省成都市锦江区汇融广场f座'', ''集电视、空调、冰箱、IT、通讯、网络、数码、芯片、能源、商用电子、电子部品、生活家电及新型平板显示器件等产业研发、生产、销售、服务为一体的多元化、综合型跨国企业集团'', ''核心'', null, ''长虹电器'');
INSERT INTO `sys_e` VALUES (''102'', ''合肥美菱股份有限公司'', ''安徽合肥'', ''合肥美菱股份有限公司是中国重要的电器制造商之一，拥有合肥、绵阳、景德镇和中山四大国内制造基地'', ''非核心'', null, ''美菱股份'');
INSERT INTO `sys_e` VALUES (''103'', ''华意压缩机股份有限公司'', ''景德镇市昌江区长虹大道1号'', ''空调核心零部件生产企业'', ''核心'', null, ''华意压缩机'');
INSERT INTO `sys_e` VALUES (''104'', ''合肥长虹美菱制冷有限公司'', ''安徽省合肥市经济技术开发区莲花路'', ''白色家电业'', ''非核心'', null, ''美菱制冷'');
INSERT INTO `sys_e` VALUES (''105'', ''长虹佳华控股有限公司'', ''成都市武侯区天府四街长虹科技大厦'', ''IT产业'', ''核心'', null, ''长虹佳华'');
INSERT INTO `sys_e` VALUES (''106'', ''长虹信息技术有限公司'', ''绵阳市科创园区创新中心'', ''IT产业'', ''核心'', null, ''长虹信息技术'');
INSERT INTO `sys_e` VALUES (''107'', ''长虹技佳精工有限公司'', ''绵阳市涪城区群文街长虹pdp工业园'', ''零部件产业'', ''非核心'', null, ''长虹技佳精工'');
INSERT INTO `sys_e` VALUES (''108'', ''长虹模塑科技有限公司'', ''四川省绵阳市涪城区绵兴东路108号'', ''零部件产业'', ''非核心'', null, ''长虹模塑'');
INSERT INTO `sys_e` VALUES (''109'', ''长虹包装印务有限公司'', ''绵阳市涪城区塘汛镇洪恩东路'', ''零部件产业'', ''非核心'', null, ''长虹包装印务'');
INSERT INTO `sys_e` VALUES (''200'', ''海尔'', ''山东省青岛市'', ''智慧家庭、工业互联网、生物医疗'', ''核心'', null, ''海尔'');
INSERT INTO `sys_e` VALUES (''201'', ''美的'', ''广东省佛山市顺德区'', ''是一家集消费电器、暖通空调、机器人与自动化系统、智能供应链、芯片产业、电梯产业的科技集团。'', ''核心'', null, ''美的'');
INSERT INTO `sys_e` VALUES (''202'', ''小米'', ''北京市海淀区'', ''专注于高端智能手机自主研发的移动互联网公司'', ''非核心'', null, ''小米'');
INSERT INTO `sys_e` VALUES (''203'', ''三星电子'', ''韩国'', ''高端制造'', ''核心'', null, ''三星'');
INSERT INTO `sys_e` VALUES (''204'', ''台积电'', ''中国台湾省的新竹市科学园区'', ''是全球第一家专业积体电路制造服务（晶圆代工foundry）企业'', ''核心'', null, ''台积电'');
INSERT INTO `sys_e` VALUES (''205'', ''Qualcomm'', ''美国加利福尼亚州'', ''是全球领先的无线科技创新者，变革了世界连接、计算和沟通的方式'', ''核心'', null, ''Qualcomm'');
INSERT INTO `sys_e` VALUES (''206'', ''华为'', ''广东省深圳市'', ''华为是全球领先的信息与通信技术（ICT）解决方案供应商，专注于ICT领域，坚持稳健经营、持续创新、开放合作'', ''核心'', null, ''华为'');
INSERT INTO `sys_e` VALUES (''207'', ''京东方科技集团股份有限公司'', ''中国北京市经济技术开发区西环中路12号'', ''为信息交互和人类健康提供智慧端口产品和专业服务的物联网公司'', ''核心'', ''11111'', ''京东方'');
INSERT INTO `sys_e` VALUES (''208'', ''京东方科技集团股份有限公司'', ''中国北京市经济技术开发区西环中路12号'', ''为信息交互和人类健康提供智慧端口产品和专业服务的物联网公司'', ''核心'', ''11111'', ''京东方'');
INSERT INTO `sys_e` VALUES (''209'', ''四川长虹电器股份有限公司'', ''四川省成都市锦江区汇融广场f座'', ''集电视、空调、冰箱、IT、通讯、网络、数码、芯片、能源、商用电子、电子部品、生活家电及新型平板显示器件等产业研发、生产、销售、服务为一体的多元化、综合型跨国企业集团'', ''核心'', null, ''长虹电器'');
INSERT INTO `sys_e` VALUES (''210'', ''四川长虹电器股份有限公司'', ''四川省成都市锦江区汇融广场f座'', ''集电视、空调、冰箱、IT、通讯、网络、数码、芯片、能源、商用电子、电子部品、生活家电及新型平板显示器件等产业研发、生产、销售、服务为一体的多元化、综合型跨国企业集团'', ''核心'', null, ''长虹电器'');
INSERT INTO `sys_e` VALUES (''217'', ''四川长虹电器股份有限公司'', ''四川省成都市锦江区汇融广场f座'', ''集电视、空调、冰箱、IT、通讯、网络、数码、芯片、能源、商用电子、电子部品、生活家电及新型平板显示器件等产业研发、生产、销售、服务为一体的多元化、综合型跨国企业集团'', ''核心'', null, ''长虹电器'');
INSERT INTO `sys_e` VALUES (''218'', ''四川长虹电器股份有限公司'', ''四川省成都市锦江区汇融广场f座'', ''集电视、空调、冰箱、IT、通讯、网络、数码、芯片、能源、商用电子、电子部品、生活家电及新型平板显示器件等产业研发、生产、销售、服务为一体的多元化、综合型跨国企业集团'', ''核心'', null, ''长虹电器'');
INSERT INTO `sys_e` VALUES (''219'', ''xxx'', ''xx'', null, ''非核心'', null, ''xx'');
INSERT INTO `sys_e` VALUES (''220'', ''长虹电器(四川)'', ''四川省绵阳市'', ''长虹电器四川生产基地'', ''核心'', null, ''长虹电器(四川)'');
INSERT INTO `sys_e` VALUES (''221'', ''长虹电器(广东)'', ''中国广东'', ''长虹电器广东生产基地'', ''非核心'', null, ''长虹电器(广东)'');
INSERT INTO `sys_e` VALUES (''222'', ''长虹电器(合肥)'', ''安徽合肥'', ''长虹电器合肥生产基地'', ''核心'', null, ''长虹电器(合肥)'');
INSERT INTO `sys_e` VALUES (''223'', ''四川长虹电器股份有限公司'', ''四川省成都市锦江区汇融广场f座'', ''长虹始创于1958年，公司前身国营长虹机器厂是我国“一五”期间的156项重点工程之一，是当时国内唯一的机载火控雷达生产基地。历经多年的发展，长虹完成由单一的军品生产到军民结合的战略转变，成为集电视、空调、冰箱、IT、通讯、网络、数码、芯片、能源、商用电子、电子部品、生活家电及新型平板显示器件等产业研发、生产、销售、服务为一体的多元化、综合型跨国企业集团，逐步成为全球具有竞争力和影响力的3C信息家电综合产品与服务提供商。2005年，长虹跨入世界品牌500强。'', ''非核心'', null, ''长虹电器'');
INSERT INTO `sys_e` VALUES (''224'', ''上海日立'', ''上海'', ''压缩机生产公司'', ''核心'', ''18780304051'', ''日立'');
INSERT INTO `sys_e` VALUES (''225'', ''彩虹集团'', ''中国'', ''电视机零部件生产'', ''核心'', ''111122'', ''彩虹'');
INSERT INTO `sys_e` VALUES (''226'', ''上海日立'', ''上海'', ''压缩机生产公司'', ''核心'', ''18780304051'', ''日立'');
INSERT INTO `sys_e` VALUES (''227'', ''彩虹集团'', ''中国'', ''电视机零部件生产'', ''非核心'', ''111122'', ''彩虹'');
INSERT INTO `sys_e` VALUES (''228'', ''上海日立'', ''上海'', ''压缩机生产公司'', ''核心'', ''18780304051'', ''日立'');
INSERT INTO `sys_e` VALUES (''229'', ''彩虹集团'', ''中国'', ''电视机零部件生产'', ''核心'', ''111122'', ''彩虹'');
INSERT INTO `sys_e` VALUES (''230'', ''上海日立'', ''上海'', ''压缩机生产公司'', ''非核心'', ''18780304051'', ''日立'');
INSERT INTO `sys_e` VALUES (''231'', ''彩虹集团'', ''中国'', ''电视机零部件生产'', ''核心'', ''111122'', ''彩虹'');

-- ----------------------------
-- Table structure for sys_ejr
-- ----------------------------
DROP TABLE IF EXISTS `sys_ejr`;
CREATE TABLE `sys_ejr` (
  `eid` int(11) NOT NULL,
  `jid` int(11) NOT NULL,
  PRIMARY KEY (`eid`,`jid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_ejr
-- ----------------------------

-- ----------------------------
-- Table structure for sys_j
-- ----------------------------
DROP TABLE IF EXISTS `sys_j`;
CREATE TABLE `sys_j` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `pname` varchar(50) DEFAULT NULL,
  `jindu` int(10) DEFAULT NULL,
  `date` varchar(50) DEFAULT NULL,
  `des` varchar(255) DEFAULT NULL,
  `ename` varchar(100) DEFAULT NULL,
  `lname` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1164 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_j
-- ----------------------------
INSERT INTO `sys_j` VALUES (''1101'', ''空调内机'', ''	\r\n长虹空调'', ''51'', ''2021-11-22'', ''长虹空调内机生产'', ''长虹信息技术有限公司'', null);
INSERT INTO `sys_j` VALUES (''1102'', ''空调外机'', ''长虹空调'', ''50'', ''2021-11-22'', ''长虹空调外机生产'', null, null);
INSERT INTO `sys_j` VALUES (''1103'', ''蒸发器(冷凝器)'', null, ''40'', ''2021-11-22'', null, null, null);
INSERT INTO `sys_j` VALUES (''1104'', ''温度传感器'', null, ''60'', ''2021-11-22'', null, null, null);
INSERT INTO `sys_j` VALUES (''1105'', ''控制板'', null, ''70'', ''2021-11-22'', null, null, null);
INSERT INTO `sys_j` VALUES (''1106'', ''空气过滤器'', null, ''80'', ''2021-11-22'', null, null, null);
INSERT INTO `sys_j` VALUES (''1107'', ''外壳'', null, ''100'', ''2021-11-22'', null, null, null);
INSERT INTO `sys_j` VALUES (''1108'', ''压缩机'', null, ''80'', ''2021-11-22'', null, null, null);
INSERT INTO `sys_j` VALUES (''1109'', ''	\r\n四方阀'', null, ''30'', ''2021-11-22'', null, null, null);
INSERT INTO `sys_j` VALUES (''1110'', ''风机'', null, ''50'', ''2021-11-22'', null, null, null);
INSERT INTO `sys_j` VALUES (''1111'', ''t1'', null, null, ''2021-1-2'', ''test'', ''合肥美菱股份有限公司'', null);
INSERT INTO `sys_j` VALUES (''1112'', ''空调内机'', ''	\r\n长虹空调'', ''51'', ''2021-11-22'', ''长虹空调内机生产'', ''长虹信息技术有限公司'', null);
INSERT INTO `sys_j` VALUES (''1113'', ''空调外机'', null, null, ''2021-11-22'', ''长虹空调外机'', null, null);
INSERT INTO `sys_j` VALUES (''1114'', ''蒸发器(冷凝器)'', null, null, ''2021-11-22'', ''这个部件可以充当这两个作用，在制热时，空调内机充当冷凝器，在夏天制冷时，内机充当蒸发器，这样就可以将室内和室外的热量进行交换。  '', null, null);
INSERT INTO `sys_j` VALUES (''1115'', ''温度传感器'', null, null, ''2021-11-22'', ''内机安装了温度传感器，可以测得管温和室内的温度，制热时当管温温度没有达到一定温度，系统不会开始吹风，这可以防止吹冷风，当温度过高时它可以控制空调自动关闭，达到保护空调的作用，而室内温度传感器则可以根据设定温度自动控制房间的温度。 '', null, null);
INSERT INTO `sys_j` VALUES (''1116'', ''控制板'', null, null, ''2021-11-22'', ''控制空调内机的运行，负责控制空调的各方面工作，使空调合理正常的运行。'', null, null);
INSERT INTO `sys_j` VALUES (''1117'', ''空气过滤器'', null, null, ''2021-11-22'', ''可以过滤掉空气中绝大数的灰尘和细菌，让进入室内的空气保持洁净。'', null, null);
INSERT INTO `sys_j` VALUES (''1118'', ''蒸发器(冷凝器)'', null, null, ''2021-11-22'', ''制热时室内就是冷凝器，室外机成了蒸发器。制冷时相反。'', null, null);
INSERT INTO `sys_j` VALUES (''1119'', ''压缩机'', null, null, ''2021-11-22'', ''是使热量从吸热部分（蒸发器）到放热部分（凝缩器）循环的泵。'', null, null);
INSERT INTO `sys_j` VALUES (''1120'', ''空调内机'', ''	\r\n长虹空调'', ''51'', ''2021-11-22'', ''长虹空调内机生产'', ''长虹信息技术有限公司'', null);
INSERT INTO `sys_j` VALUES (''1121'', ''空调外机'', ''长虹空调'', null, ''2021-11-22'', ''长虹空调外机'', null, '''');
INSERT INTO `sys_j` VALUES (''1122'', ''蒸发器(冷凝器)'', ''空调内机'', null, ''2021-11-22'', ''这个部件可以充当这两个作用，在制热时，空调内机充当冷凝器，在夏天制冷时，内机充当蒸发器，这样就可以将室内和室外的热量进行交换。  '', null, '''');
INSERT INTO `sys_j` VALUES (''1123'', ''温度传感器'', ''空调内机'', null, ''2021-11-22'', ''内机安装了温度传感器，可以测得管温和室内的温度，制热时当管温温度没有达到一定温度，系统不会开始吹风，这可以防止吹冷风，当温度过高时它可以控制空调自动关闭，达到保护空调的作用，而室内温度传感器则可以根据设定温度自动控制房间的温度。 '', null, '''');
INSERT INTO `sys_j` VALUES (''1124'', ''控制板'', ''空调内机'', null, ''2021-11-22'', ''控制空调内机的运行，负责控制空调的各方面工作，使空调合理正常的运行。'', null, '''');
INSERT INTO `sys_j` VALUES (''1125'', ''空气过滤器'', ''空调内机'', null, ''2021-11-22'', ''可以过滤掉空气中绝大数的灰尘和细菌，让进入室内的空气保持洁净。'', null, '''');
INSERT INTO `sys_j` VALUES (''1126'', ''蒸发器(冷凝器)'', ''空调外机'', null, ''2021-11-22'', ''制热时室内就是冷凝器，室外机成了蒸发器。制冷时相反。'', null, '''');
INSERT INTO `sys_j` VALUES (''1127'', ''压缩机'', ''空调外机'', null, ''2021-11-22'', ''是使热量从吸热部分（蒸发器）到放热部分（凝缩器）循环的泵。'', null, '''');
INSERT INTO `sys_j` VALUES (''1128'', ''空调内机'', ''	\r\n长虹空调'', ''51'', ''2021-11-22'', ''长虹空调内机生产'', ''长虹信息技术有限公司'', null);
INSERT INTO `sys_j` VALUES (''1129'', ''空调外机'', ''长虹空调'', null, ''2021-11-22'', ''长虹空调外机'', null, ''空调'');
INSERT INTO `sys_j` VALUES (''1130'', ''蒸发器(冷凝器)'', ''空调内机'', null, ''2021-11-22'', ''这个部件可以充当这两个作用，在制热时，空调内机充当冷凝器，在夏天制冷时，内机充当蒸发器，这样就可以将室内和室外的热量进行交换。  '', null, ''空调'');
INSERT INTO `sys_j` VALUES (''1131'', ''温度传感器'', ''空调内机'', null, ''2021-11-22'', ''内机安装了温度传感器，可以测得管温和室内的温度，制热时当管温温度没有达到一定温度，系统不会开始吹风，这可以防止吹冷风，当温度过高时它可以控制空调自动关闭，达到保护空调的作用，而室内温度传感器则可以根据设定温度自动控制房间的温度。 '', null, ''空调'');
INSERT INTO `sys_j` VALUES (''1132'', ''控制板'', ''空调内机'', null, ''2021-11-22'', ''控制空调内机的运行，负责控制空调的各方面工作，使空调合理正常的运行。'', null, ''空调'');
INSERT INTO `sys_j` VALUES (''1133'', ''空气过滤器'', ''空调内机'', null, ''2021-11-22'', ''可以过滤掉空气中绝大数的灰尘和细菌，让进入室内的空气保持洁净。'', null, ''空调'');
INSERT INTO `sys_j` VALUES (''1134'', ''蒸发器(冷凝器)'', ''空调外机'', null, ''2021-11-22'', ''制热时室内就是冷凝器，室外机成了蒸发器。制冷时相反。'', null, ''空调'');
INSERT INTO `sys_j` VALUES (''1135'', ''压缩机'', ''空调外机'', null, ''2021-11-22'', ''是使热量从吸热部分（蒸发器）到放热部分（凝缩器）循环的泵。'', null, ''空调'');
INSERT INTO `sys_j` VALUES (''1136'', ''空调内机'', ''	\r\n长虹空调'', ''51'', ''2021-11-22'', ''长虹空调内机生产'', ''长虹信息技术有限公司'', null);
INSERT INTO `sys_j` VALUES (''1137'', ''空调外机'', ''长虹空调'', ''97'', ''2021-11-22'', ''长虹空调外机'', null, '''');
INSERT INTO `sys_j` VALUES (''1138'', ''蒸发器(冷凝器)'', ''空调内机'', ''39'', ''2021-11-22'', ''这个部件可以充当这两个作用，在制热时，空调内机充当冷凝器，在夏天制冷时，内机充当蒸发器，这样就可以将室内和室外的热量进行交换。  '', null, '''');
INSERT INTO `sys_j` VALUES (''1139'', ''温度传感器'', ''空调内机'', ''27'', ''2021-11-22'', ''内机安装了温度传感器，可以测得管温和室内的温度，制热时当管温温度没有达到一定温度，系统不会开始吹风，这可以防止吹冷风，当温度过高时它可以控制空调自动关闭，达到保护空调的作用，而室内温度传感器则可以根据设定温度自动控制房间的温度。 '', null, '''');
INSERT INTO `sys_j` VALUES (''1140'', ''控制板'', ''空调内机'', ''64'', ''2021-11-22'', ''控制空调内机的运行，负责控制空调的各方面工作，使空调合理正常的运行。'', null, '''');
INSERT INTO `sys_j` VALUES (''1141'', ''空气过滤器'', ''空调内机'', ''51'', ''2021-11-22'', ''可以过滤掉空气中绝大数的灰尘和细菌，让进入室内的空气保持洁净。'', null, '''');
INSERT INTO `sys_j` VALUES (''1142'', ''蒸发器(冷凝器)'', ''空调外机'', ''88'', ''2021-11-22'', ''制热时室内就是冷凝器，室外机成了蒸发器。制冷时相反。'', null, '''');
INSERT INTO `sys_j` VALUES (''1143'', ''压缩机'', ''空调外机'', ''12'', ''2021-11-22'', ''是使热量从吸热部分（蒸发器）到放热部分（凝缩器）循环的泵。'', null, '''');
INSERT INTO `sys_j` VALUES (''1144'', ''空调内机'', ''	\r\n长虹空调'', ''51'', ''2021-11-22'', ''长虹空调内机生产'', ''长虹信息技术有限公司'', null);
INSERT INTO `sys_j` VALUES (''1145'', ''空调外机'', ''长虹空调'', ''46'', ''2021-11-22'', ''长虹空调外机'', ''长虹'', '''');
INSERT INTO `sys_j` VALUES (''1146'', ''蒸发器(冷凝器)'', ''空调内机'', ''28'', ''2021-11-22'', ''这个部件可以充当这两个作用，在制热时，空调内机充当冷凝器，在夏天制冷时，内机充当蒸发器，这样就可以将室内和室外的热量进行交换。  '', ''华意压缩机股份有限公司'', '''');
INSERT INTO `sys_j` VALUES (''1147'', ''温度传感器'', ''空调内机'', ''84'', ''2021-11-22'', ''内机安装了温度传感器，可以测得管温和室内的温度，制热时当管温温度没有达到一定温度，系统不会开始吹风，这可以防止吹冷风，当温度过高时它可以控制空调自动关闭，达到保护空调的作用，而室内温度传感器则可以根据设定温度自动控制房间的温度。 '', ''长虹佳华控股有限公司'', '''');
INSERT INTO `sys_j` VALUES (''1148'', ''控制板'', ''空调内机'', ''73'', ''2021-11-22'', ''控制空调内机的运行，负责控制空调的各方面工作，使空调合理正常的运行。'', ''长虹技佳精工有限公司'', '''');
INSERT INTO `sys_j` VALUES (''1149'', ''空气过滤器'', ''空调内机'', ''33'', ''2021-11-22'', ''可以过滤掉空气中绝大数的灰尘和细菌，让进入室内的空气保持洁净。'', ''长虹'', '''');
INSERT INTO `sys_j` VALUES (''1150'', ''蒸发器(冷凝器)'', ''空调外机'', ''72'', ''2021-11-22'', ''制热时室内就是冷凝器，室外机成了蒸发器。制冷时相反。'', ''华意压缩机股份有限公司'', '''');
INSERT INTO `sys_j` VALUES (''1151'', ''压缩机'', ''空调外机'', ''4'', ''2021-11-22'', ''是使热量从吸热部分（蒸发器）到放热部分（凝缩器）循环的泵。'', ''华意压缩机股份有限公司'', '''');
INSERT INTO `sys_j` VALUES (''1152'', ''空调外机'', ''长虹空调'', ''50'', ''2021-11-22'', ''长虹空调外机生产'', ''合肥长虹美菱制冷有限公司'', null);
INSERT INTO `sys_j` VALUES (''1154'', ''空调内机'', ''长虹空调'', ''73'', ''2021-11-22'', ''长虹空调内机'', ''长虹'', ''电视机'');
INSERT INTO `sys_j` VALUES (''1155'', ''空调外机'', ''长虹空调'', ''94'', ''2021-11-22'', ''长虹空调外机'', ''长虹'', ''电视机'');
INSERT INTO `sys_j` VALUES (''1156'', ''蒸发器(冷凝器)'', ''空调内机'', ''57'', ''2021-11-22'', ''这个部件可以充当这两个作用，在制热时，空调内机充当冷凝器，在夏天制冷时，内机充当蒸发器，这样就可以将室内和室外的热量进行交换。  '', ''华意压缩机股份有限公司'', ''电视机'');
INSERT INTO `sys_j` VALUES (''1157'', ''温度传感器'', ''空调内机'', ''63'', ''2021-11-22'', ''内机安装了温度传感器，可以测得管温和室内的温度，制热时当管温温度没有达到一定温度，系统不会开始吹风，这可以防止吹冷风，当温度过高时它可以控制空调自动关闭，达到保护空调的作用，而室内温度传感器则可以根据设定温度自动控制房间的温度。 '', ''长虹佳华控股有限公司'', ''电视机'');
INSERT INTO `sys_j` VALUES (''1158'', ''控制板'', ''空调内机'', ''54'', ''2021-11-22'', ''控制空调内机的运行，负责控制空调的各方面工作，使空调合理正常的运行。'', ''长虹技佳精工有限公司'', ''电视机'');
INSERT INTO `sys_j` VALUES (''1159'', ''空气过滤器'', ''空调内机'', ''79'', ''2021-11-22'', ''可以过滤掉空气中绝大数的灰尘和细菌，让进入室内的空气保持洁净。'', ''长虹'', ''电视机'');
INSERT INTO `sys_j` VALUES (''1160'', ''蒸发器(冷凝器)'', ''空调外机'', ''80'', ''2021-11-22'', ''制热时室内就是冷凝器，室外机成了蒸发器。制冷时相反。'', ''华意压缩机股份有限公司'', ''电视机'');
INSERT INTO `sys_j` VALUES (''1161'', ''压缩机'', ''空调外机'', ''93'', ''2021-11-22'', ''是使热量从吸热部分（蒸发器）到放热部分（凝缩器）循环的泵。'', ''华意压缩机股份有限公司'', ''电视机'');
INSERT INTO `sys_j` VALUES (''1162'', ''冷凝器(内)'', ''空调内机'', ''53'', ''2021-11-22'', ''这个部件可以充当这两个作用，在制热时，空调内机充当冷凝器，在夏天制冷时，内机充当蒸发器，这样就可以将室内和室外的热量进行交换。  '', ''华意压缩机股份有限公司'', '''');
INSERT INTO `sys_j` VALUES (''1163'', ''控制板'', ''空调内机'', ''78'', ''44594'', ''控制空调内机的运行，负责控制空调的各方面工作，使空调合理正常的运行。'', ''长虹技佳精工有限公司'', ''空调'');

-- ----------------------------
-- Table structure for sys_l
-- ----------------------------
DROP TABLE IF EXISTS `sys_l`;
CREATE TABLE `sys_l` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `des` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_l
-- ----------------------------
INSERT INTO `sys_l` VALUES (''1'', ''空调'', ''空调生产价值链'');
INSERT INTO `sys_l` VALUES (''2'', ''电视机'', ''电视机生产价值链'');

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `css` varchar(30) DEFAULT NULL,
  `href` varchar(1000) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `permission` varchar(50) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES (''1'', ''0'', ''用户管理'', ''fa-users'', null, ''1'', null, ''1'');
INSERT INTO `sys_permission` VALUES (''2'', ''1'', ''用户查询'', ''fa-user'', ''/api/getPage?pageName'', ''1'', null, ''2'');
INSERT INTO `sys_permission` VALUES (''3'', ''2'', ''查询'', null, null, ''2'', ''sys:user:query'', ''100'');
INSERT INTO `sys_permission` VALUES (''4'', ''2'', ''新增'', null, null, ''2'', ''sys:user:add'', ''100'');
INSERT INTO `sys_permission` VALUES (''5'', ''2'', ''删除'', null, null, ''2'', ''sys:user:del'', ''100'');
INSERT INTO `sys_permission` VALUES (''6'', ''1'', ''修改密码'', ''fa-pencil-square-o'', ''/api/getPage?pageName'', ''1'', ''sys:user:password'', ''4'');
INSERT INTO `sys_permission` VALUES (''7'', ''0'', ''系统设置'', ''fa-gears'', null, ''1'', null, ''5'');
INSERT INTO `sys_permission` VALUES (''8'', ''7'', ''菜单'', ''fa-cog'', ''/api/getPage?pageName'', ''1'', null, ''6'');
INSERT INTO `sys_permission` VALUES (''9'', ''8'', ''查询'', null, null, ''2'', ''sys:menu:query'', ''100'');
INSERT INTO `sys_permission` VALUES (''10'', ''8'', ''新增'', null, null, ''2'', ''sys:menu:add'', ''100'');
INSERT INTO `sys_permission` VALUES (''11'', ''8'', ''删除'', null, null, ''2'', ''sys:menu:del'', ''100'');
INSERT INTO `sys_permission` VALUES (''12'', ''7'', ''角色'', ''fa-user-secret'', ''/api/getPage?pageName'', ''1'', null, ''7'');
INSERT INTO `sys_permission` VALUES (''13'', ''12'', ''查询'', null, null, ''2'', ''sys:role:query'', ''100'');
INSERT INTO `sys_permission` VALUES (''14'', ''12'', ''新增'', null, null, ''2'', ''sys:role:add'', ''100'');
INSERT INTO `sys_permission` VALUES (''15'', ''12'', ''删除'', null, null, ''2'', ''sys:role:del'', ''100'');
INSERT INTO `sys_permission` VALUES (''16'', ''0'', ''文件管理'', ''fa-folder-open'', ''/api/getPage?pageName'', ''1'', null, ''8'');
INSERT INTO `sys_permission` VALUES (''17'', ''16'', ''查询'', null, null, ''2'', ''sys:file:query'', ''100'');
INSERT INTO `sys_permission` VALUES (''18'', ''16'', ''删除'', null, null, ''2'', ''sys:file:del'', ''100'');
INSERT INTO `sys_permission` VALUES (''19'', ''0'', ''数据源监控'', ''fa-eye'', ''druid/index.html'', ''1'', null, ''9'');
INSERT INTO `sys_permission` VALUES (''20'', ''0'', ''接口swagger'', ''fa-file-pdf-o'', ''swagger-ui.html'', ''1'', null, ''10'');
INSERT INTO `sys_permission` VALUES (''21'', ''0'', ''代码生成'', ''fa-wrench'', ''/api/getPage?pageName'', ''1'', ''generate:edit'', ''11'');
INSERT INTO `sys_permission` VALUES (''22'', ''0'', ''日志查询'', ''fa-reoder'', ''/api/getPage?pageName'', ''1'', ''sys:log:query'', ''13'');
INSERT INTO `sys_permission` VALUES (''23'', ''8'', ''修改'', null, null, ''2'', ''sys:menu:edit'', ''100'');
INSERT INTO `sys_permission` VALUES (''24'', ''12'', ''修改'', null, null, ''2'', ''sys:role:edit'', ''100'');
INSERT INTO `sys_permission` VALUES (''25'', ''2'', ''修改'', null, null, ''2'', ''sys:user:edit'', ''100'');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (''1'', ''admin'', '''', ''2020-11-26 19:21:15'', ''2020-11-26 19:21:19'');
INSERT INTO `sys_role` VALUES (''2'', ''user'', null, ''2020-11-25 19:21:43'', ''2020-11-26 19:21:48'');
INSERT INTO `sys_role` VALUES (''3'', ''teacher'', null, ''2020-11-03 19:22:02'', ''2020-11-18 19:22:06'');
INSERT INTO `sys_role` VALUES (''4'', ''test'', ''test'', ''2020-12-02 15:02:53'', ''2020-12-02 07:52:17'');

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
  `roleId` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  PRIMARY KEY (`roleId`,`permissionId`),
  KEY `fk_permissionId` (`permissionId`),
  CONSTRAINT `fk_permissionId` FOREIGN KEY (`permissionId`) REFERENCES `sys_permission` (`id`),
  CONSTRAINT `fk_role` FOREIGN KEY (`roleId`) REFERENCES `sys_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES (''1'', ''1'');
INSERT INTO `sys_role_permission` VALUES (''2'', ''1'');
INSERT INTO `sys_role_permission` VALUES (''3'', ''1'');
INSERT INTO `sys_role_permission` VALUES (''4'', ''1'');
INSERT INTO `sys_role_permission` VALUES (''1'', ''2'');
INSERT INTO `sys_role_permission` VALUES (''3'', ''2'');
INSERT INTO `sys_role_permission` VALUES (''4'', ''2'');
INSERT INTO `sys_role_permission` VALUES (''1'', ''3'');
INSERT INTO `sys_role_permission` VALUES (''3'', ''3'');
INSERT INTO `sys_role_permission` VALUES (''4'', ''3'');
INSERT INTO `sys_role_permission` VALUES (''1'', ''4'');
INSERT INTO `sys_role_permission` VALUES (''3'', ''4'');
INSERT INTO `sys_role_permission` VALUES (''4'', ''4'');
INSERT INTO `sys_role_permission` VALUES (''1'', ''5'');
INSERT INTO `sys_role_permission` VALUES (''3'', ''5'');
INSERT INTO `sys_role_permission` VALUES (''4'', ''5'');
INSERT INTO `sys_role_permission` VALUES (''1'', ''6'');
INSERT INTO `sys_role_permission` VALUES (''2'', ''6'');
INSERT INTO `sys_role_permission` VALUES (''3'', ''6'');
INSERT INTO `sys_role_permission` VALUES (''1'', ''7'');
INSERT INTO `sys_role_permission` VALUES (''3'', ''7'');
INSERT INTO `sys_role_permission` VALUES (''4'', ''7'');
INSERT INTO `sys_role_permission` VALUES (''1'', ''8'');
INSERT INTO `sys_role_permission` VALUES (''3'', ''8'');
INSERT INTO `sys_role_permission` VALUES (''1'', ''9'');
INSERT INTO `sys_role_permission` VALUES (''4'', ''12'');
INSERT INTO `sys_role_permission` VALUES (''4'', ''13'');
INSERT INTO `sys_role_permission` VALUES (''4'', ''14'');
INSERT INTO `sys_role_permission` VALUES (''4'', ''15'');
INSERT INTO `sys_role_permission` VALUES (''4'', ''24'');
INSERT INTO `sys_role_permission` VALUES (''4'', ''25'');

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`userId`,`roleId`),
  KEY `fk_roleId` (`roleId`),
  CONSTRAINT `fk_roleId` FOREIGN KEY (`roleId`) REFERENCES `sys_role` (`id`),
  CONSTRAINT `fk_userId` FOREIGN KEY (`userId`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES (''1'', ''1'');
INSERT INTO `sys_role_user` VALUES (''2'', ''2'');
INSERT INTO `sys_role_user` VALUES (''4'', ''2'');
INSERT INTO `sys_role_user` VALUES (''6'', ''2'');
INSERT INTO `sys_role_user` VALUES (''7'', ''2'');
INSERT INTO `sys_role_user` VALUES (''9'', ''2'');
INSERT INTO `sys_role_user` VALUES (''10'', ''2'');
INSERT INTO `sys_role_user` VALUES (''11'', ''2'');
INSERT INTO `sys_role_user` VALUES (''12'', ''2'');
INSERT INTO `sys_role_user` VALUES (''3'', ''3'');
INSERT INTO `sys_role_user` VALUES (''8'', ''3'');

-- ----------------------------
-- Table structure for sys_st1
-- ----------------------------
DROP TABLE IF EXISTS `sys_st1`;
CREATE TABLE `sys_st1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `maxp` int(11) DEFAULT NULL,
  `pc` int(11) DEFAULT NULL,
  `fc` int(11) DEFAULT NULL,
  `flag` int(11) DEFAULT ''1'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_st1
-- ----------------------------
INSERT INTO `sys_st1` VALUES (''9'', ''长虹电器(四川)'', ''4000'', ''1550'', ''50'', ''1'');
INSERT INTO `sys_st1` VALUES (''10'', ''长虹电器(广东)'', ''3000'', ''1650'', ''70'', ''1'');
INSERT INTO `sys_st1` VALUES (''11'', ''长虹电器(合肥)'', ''3600'', ''1600'', ''60'', ''1'');

-- ----------------------------
-- Table structure for sys_st2
-- ----------------------------
DROP TABLE IF EXISTS `sys_st2`;
CREATE TABLE `sys_st2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lname` varchar(255) DEFAULT NULL,
  `lnum` int(11) DEFAULT NULL,
  `lprice` int(11) DEFAULT NULL,
  `flag` int(11) DEFAULT ''1'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_st2
-- ----------------------------
INSERT INTO `sys_st2` VALUES (''9'', ''四川销售中心'', ''2000'', ''1800'', ''1'');
INSERT INTO `sys_st2` VALUES (''10'', ''广西销售中心'', ''1500'', ''1850'', ''1'');
INSERT INTO `sys_st2` VALUES (''11'', ''上海销售中心'', ''2000'', ''1950'', ''1'');
INSERT INTO `sys_st2` VALUES (''12'', ''湖南销售中心'', ''1800'', ''1850'', ''1'');

-- ----------------------------
-- Table structure for sys_st3
-- ----------------------------
DROP TABLE IF EXISTS `sys_st3`;
CREATE TABLE `sys_st3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sname` varchar(255) DEFAULT NULL,
  `ename` varchar(255) DEFAULT NULL,
  `tprice` double(11,2) DEFAULT NULL,
  `dist` int(11) DEFAULT NULL,
  `flag` int(11) DEFAULT ''1'',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_st3
-- ----------------------------
INSERT INTO `sys_st3` VALUES (''9'', ''长虹电器(四川)'', ''四川销售中心'', ''0.10'', ''100'', ''1'');
INSERT INTO `sys_st3` VALUES (''10'', ''长虹电器(四川)'', ''广西销售中心'', ''0.10'', ''500'', ''1'');
INSERT INTO `sys_st3` VALUES (''11'', ''长虹电器(四川)'', ''上海销售中心'', ''0.10'', ''2000'', ''1'');
INSERT INTO `sys_st3` VALUES (''12'', ''长虹电器(四川)'', ''湖南销售中心'', ''0.10'', ''1000'', ''1'');
INSERT INTO `sys_st3` VALUES (''13'', ''长虹电器(广东)'', ''四川销售中心'', ''0.10'', ''1800'', ''1'');
INSERT INTO `sys_st3` VALUES (''14'', ''长虹电器(广东)'', ''广西销售中心'', ''0.10'', ''300'', ''1'');
INSERT INTO `sys_st3` VALUES (''15'', ''长虹电器(广东)'', ''上海销售中心'', ''0.10'', ''420'', ''1'');
INSERT INTO `sys_st3` VALUES (''16'', ''长虹电器(广东)'', ''湖南销售中心'', ''0.10'', ''800'', ''1'');
INSERT INTO `sys_st3` VALUES (''17'', ''长虹电器(合肥)'', ''四川销售中心'', ''0.10'', ''1200'', ''1'');
INSERT INTO `sys_st3` VALUES (''18'', ''长虹电器(合肥)'', ''广西销售中心'', ''0.10'', ''700'', ''1'');
INSERT INTO `sys_st3` VALUES (''19'', ''长虹电器(合肥)'', ''上海销售中心'', ''0.10'', ''600'', ''1'');
INSERT INTO `sys_st3` VALUES (''20'', ''长虹电器(合肥)'', ''湖南销售中心'', ''0.10'', ''900'', ''1'');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `headImgUrl` varchar(255) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `telephone` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `sex` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (''1'', ''admin'', ''123456'', ''管理员'', null, ''18780304051'', ''111111'', null, ''2020-11-03'', ''1'', ''1'', ''2020-11-25 14:50:24'', ''2020-11-25 14:50:41'');
INSERT INTO `sys_user` VALUES (''2'', ''111'', ''7fa8282ad93047a4d6fe6111c93b308a'', ''biu'', null, ''12345678911'', ''12345678911'', ''1@qq.com'', ''2020-11-24'', ''1'', ''1'', ''2020-11-26 21:33:12'', ''2020-11-26 21:33:12'');
INSERT INTO `sys_user` VALUES (''3'', ''111111'', ''96e79218965eb72c92a549dd5a330112'', ''111111'', null, ''11111111111'', ''11111111111'', ''11@qq.com'', null, ''1'', ''1'', ''2020-11-27 14:56:38'', ''2020-11-27 14:56:38'');
INSERT INTO `sys_user` VALUES (''4'', ''123'', ''e10adc3949ba59abbe56e057f20f883e'', ''123'', null, ''12345678921'', null, ''11@cc.com'', ''2020-10-31'', ''1'', ''1'', ''2020-11-27 15:02:32'', ''2020-11-27 15:02:32'');
INSERT INTO `sys_user` VALUES (''5'', ''11'', ''11'', ''11'', null, ''11'', ''11'', ''111'', ''2020-11-04'', ''0'', ''1'', ''2020-11-30 13:19:07'', ''2020-11-30 13:19:12'');
INSERT INTO `sys_user` VALUES (''6'', ''123'', ''121'', ''132'', null, ''123'', ''123'', ''123'', ''2020-11-18'', ''0'', ''1'', ''2020-11-30 13:20:30'', ''2020-11-30 13:20:34'');
INSERT INTO `sys_user` VALUES (''7'', ''1234'', ''1234'', ''1234'', null, ''1234'', ''1234'', ''1234'', ''2020-11-05'', ''1'', ''1'', ''2020-11-30 13:21:10'', ''2020-11-30 13:21:13'');
INSERT INTO `sys_user` VALUES (''8'', ''12345'', ''12345'', ''12345'', null, ''12345'', ''12345'', ''12345'', ''2020-11-04'', ''1'', ''1'', ''2020-11-11 13:21:39'', ''2020-11-17 13:21:42'');
INSERT INTO `sys_user` VALUES (''9'', ''121'', ''121'', ''1211'', null, ''121'', ''121'', ''121'', ''2020-07-30'', ''1'', ''1'', ''2020-11-17 13:22:12'', ''2020-11-30 13:22:16'');
INSERT INTO `sys_user` VALUES (''10'', ''112'', ''112'', ''112'', null, ''112'', ''112'', ''121'', ''2020-10-28'', ''1'', ''1'', ''2020-11-30 13:22:48'', ''2020-11-30 13:22:51'');
INSERT INTO `sys_user` VALUES (''11'', ''1111111'', ''1211'', ''1211'', null, ''12111111111'', ''1211'', ''1211@qq.com'', ''2020-10-31'', ''0'', ''1'', ''2020-11-30 13:23:28'', ''2020-12-01 06:20:35'');
INSERT INTO `sys_user` VALUES (''12'', ''123121'', ''ef1dc416e22dd93120421fab1a338f31'', ''123121'', null, ''11111111112'', null, ''11@11.com'', ''2020-11-26'', null, ''1'', ''2020-12-01 14:09:59'', ''2020-12-01 06:19:52'');

-- ----------------------------
-- Table structure for sys_warn
-- ----------------------------
DROP TABLE IF EXISTS `sys_warn`;
CREATE TABLE `sys_warn` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `info` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  `sdate` datetime DEFAULT NULL,
  `edate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_warn
-- ----------------------------
INSERT INTO `sys_warn` VALUES (''19'', ''冷凝器(内)'', ''机器故障'', ''生产线上机器出现故障'', ''任务延期三至五天交付'', ''2022-03-10 00:00:00'', ''2022-03-31 00:00:00'');
INSERT INTO `sys_warn` VALUES (''20'', ''冷凝器(内)'', ''机器故障'', ''生产线上机器出现故障'', ''任务延期三至五天交付'', ''2022-03-10 00:00:00'', ''2022-03-31 00:00:00'');

-- ----------------------------
-- Table structure for zjx_jg
-- ----------------------------
DROP TABLE IF EXISTS `zjx_jg`;
CREATE TABLE `zjx_jg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sn` varchar(255) DEFAULT NULL,
  `zzsn` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `pjtime` varchar(255) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `enterpise` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zjx_jg
-- ----------------------------
INSERT INTO `zjx_jg` VALUES (''1'', ''359234'', ''C5'', ''冷暖型1匹柜式空调'', ''2021/5/3\r\n'', ''74'', ''T1、C2、C6'');
INSERT INTO `zjx_jg` VALUES (''2'', ''375320'', ''C3'', ''冷暖型2匹柜式空调\r\n'', ''2021/5/3\r\n'', ''78'', ''T9、C2、C9'');
INSERT INTO `zjx_jg` VALUES (''3'', ''375321'', ''C10'', ''单冷型1匹柜式空调\r\n'', ''2021/9/22\r\n'', ''94'', ''T7、C1、C4'');
INSERT INTO `zjx_jg` VALUES (''4'', ''375412'', ''C20'', ''冷暖电辅型3匹柜式空调\r\n'', ''2021/9/24\r\n'', ''95\r\n'', ''T5、C18、C3'');
INSERT INTO `zjx_jg` VALUES (''5'', ''346343'', ''C11'', ''冷暖电辅型1匹柜式空调\r\n'', ''2021/11/1\r\n'', ''77\r\n'', ''T3、C6、C9'');
INSERT INTO `zjx_jg` VALUES (''6'', ''354731'', ''C20'', ''单冷型1匹壁挂式空调\r\n'', ''2021/11/9\r\n'', ''99\r\n'', ''T2、C13、C5'');
INSERT INTO `zjx_jg` VALUES (''7'', ''346742'', ''C17'', ''单冷型2匹壁挂式空调\r\n'', ''2021/12/30\r\n'', ''81\r\n'', ''T10、C20、C11'');
INSERT INTO `zjx_jg` VALUES (''8'', ''384683'', ''C15'', ''冷暖型2匹壁挂式空调\r\n'', ''2021/12/31\r\n'', ''78'', ''T8、C17、C14'');
INSERT INTO `zjx_jg` VALUES (''9'', ''324787'', ''C9'', ''冷暖电辅型中央空调\r\n'', ''2022/1/2\r\n'', ''89\r\n'', ''T4、C9、C7'');
INSERT INTO `zjx_jg` VALUES (''10'', ''336768'', ''C7'', ''冷暖型2匹柜式空调\r\n'', ''2022/1/10\r\n'', ''83\r\n'', ''T6、C5、C1'');

-- ----------------------------
-- Table structure for zjx_lldy
-- ----------------------------
DROP TABLE IF EXISTS `zjx_lldy`;
CREATE TABLE `zjx_lldy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` varchar(40) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `etype` varchar(255) DEFAULT NULL,
  `enterpise` varchar(255) DEFAULT NULL,
  `zl` varchar(255) DEFAULT NULL,
  `atime` varchar(100) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zjx_lldy
-- ----------------------------
INSERT INTO `zjx_lldy` VALUES (''1'', ''C1'', ''数控滚齿机'', ''制造加工能力'', ''上海XX科技公司'', ''84'', ''3-5天'', ''闲置中'');
INSERT INTO `zjx_lldy` VALUES (''2'', ''C2'', ''数控滚齿机'', ''采购能力'', ''上海XX科技公司'', ''78'', ''1-3天'', ''使用中'');
INSERT INTO `zjx_lldy` VALUES (''3'', ''C3'', ''数控滚齿机'', ''制造加工能力'', ''上海XX科技公司'', ''94'', ''6-8天'', ''使用中'');
INSERT INTO `zjx_lldy` VALUES (''4'', ''C4'', ''数控滚齿机'', ''研发设计能力'', ''上海XX科技公司'', ''79'', ''4-6天'', ''闲置中'');

-- ----------------------------
-- Table structure for zjx_pjjg
-- ----------------------------
DROP TABLE IF EXISTS `zjx_pjjg`;
CREATE TABLE `zjx_pjjg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sn` varchar(255) DEFAULT NULL,
  `pjsn` varchar(255) DEFAULT NULL,
  `pjtime` varchar(255) DEFAULT NULL,
  `enterpise` varchar(255) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `pass` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zjx_pjjg
-- ----------------------------
INSERT INTO `zjx_pjjg` VALUES (''1'', ''660068'', ''	\r\nBK23189Q232'', ''	\r\n2021/6/12'', ''	\r\n上海XX科技公司'', ''	\r\n89'', ''	\r\n是'');
INSERT INTO `zjx_pjjg` VALUES (''2'', ''660069'', ''BK16548WS865'', ''	\r\n2021/6/12'', ''	\r\n上海XX科技公司'', ''48'', ''否'');
INSERT INTO `zjx_pjjg` VALUES (''3'', ''660070'', ''BK23189Q232'', ''2020/4/18'', ''	\r\n上海XX科技公司'', ''	\r\n89'', ''是'');
INSERT INTO `zjx_pjjg` VALUES (''4'', ''660071'', ''	\r\nBK16548WS865'', ''2020/4/18'', ''	\r\n上海XX科技公司'', ''	\r\n48'', ''否'');

-- ----------------------------
-- Table structure for zjx_qypj
-- ----------------------------
DROP TABLE IF EXISTS `zjx_qypj`;
CREATE TABLE `zjx_qypj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sn` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `zz` varchar(255) DEFAULT NULL,
  `xz` varchar(255) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zjx_qypj
-- ----------------------------
INSERT INTO `zjx_qypj` VALUES (''1'', ''AA1'', ''	\r\n完成时间'', ''	\r\n产品竞争力'', ''逆向指标'', ''	\r\n86'');
INSERT INTO `zjx_qypj` VALUES (''2'', ''AA2'', ''运输时间'', ''	\r\n产品竞争力'', ''逆向指标'', ''78'');
INSERT INTO `zjx_qypj` VALUES (''3'', ''AA3'', ''产品成本'', ''	\r\n产品竞争力'', ''逆向指标'', ''62'');
INSERT INTO `zjx_qypj` VALUES (''4'', ''AB1'', ''质量管理水平'', ''企业综合实力'', ''正向指标'', ''85'');
INSERT INTO `zjx_qypj` VALUES (''5'', ''AC1'', ''信息化水平'', ''合作潜力'', ''正向指标'', ''65'');
INSERT INTO `zjx_qypj` VALUES (''6'', ''AC2'', ''管理人员素质'', ''合作潜力'', ''正向指标'', ''79'');
INSERT INTO `zjx_qypj` VALUES (''7'', ''AD1'', ''	\r\n信息共享程度'', ''	\r\n协作潜力'', ''正向指标'', ''90'');
INSERT INTO `zjx_qypj` VALUES (''8'', ''AD2'', ''	\r\n交货准时率'', ''	\r\n协作潜力'', ''正向指标'', ''86'');
INSERT INTO `zjx_qypj` VALUES (''9'', ''AD3'', ''	\r\n战略目标匹配度'', ''	\r\n协作潜力'', ''正向指标'', ''59'');
INSERT INTO `zjx_qypj` VALUES (''10'', ''AD4'', ''	\r\n地理位置'', ''	\r\n协作潜力'', ''正向指标'', ''70'');

-- ----------------------------
-- Table structure for zjx_qypjmb
-- ----------------------------
DROP TABLE IF EXISTS `zjx_qypjmb`;
CREATE TABLE `zjx_qypjmb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sn` varchar(255) DEFAULT NULL,
  `enterpise` varchar(255) DEFAULT NULL,
  `ctime` varchar(255) DEFAULT NULL,
  `mtime` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zjx_qypjmb
-- ----------------------------
INSERT INTO `zjx_qypjmb` VALUES (''1'', ''660068'', ''	\r\n制造加工型'', ''	\r\n2020/12/31'', ''	\r\n2021/5/30'');
INSERT INTO `zjx_qypjmb` VALUES (''2'', ''660070'', ''	\r\n物流运输型'', ''	\r\n2020/11/23'', ''	\r\n2021/1/12'');
INSERT INTO `zjx_qypjmb` VALUES (''3'', ''662368'', ''	\r\n设计研发型'', ''	\r\n2020/9/16'', ''	\r\n2021/3/24'');
INSERT INTO `zjx_qypjmb` VALUES (''4'', ''652438'', ''	\r\n装配总装'', ''	\r\n2020/12/2'', ''	\r\n2021/7/2'');

-- ----------------------------
-- Table structure for zjx_yxpp
-- ----------------------------
DROP TABLE IF EXISTS `zjx_yxpp`;
CREATE TABLE `zjx_yxpp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `csn` varchar(255) DEFAULT NULL,
  `cname` varchar(255) DEFAULT NULL,
  `wctime` varchar(255) DEFAULT NULL,
  `pjob` varchar(255) DEFAULT NULL,
  `sn` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zjx_yxpp
-- ----------------------------
INSERT INTO `zjx_yxpp` VALUES (''1'', ''A1'', ''底座设计'', ''	\r\n2021/12/23'', ''	\r\nP1'', ''	\r\nC5'');
INSERT INTO `zjx_yxpp` VALUES (''2'', ''A2'', ''	\r\n底座原材料采购'', ''	\r\n2021/12/26'', ''	\r\nA1'', ''	\r\nC4'');
INSERT INTO `zjx_yxpp` VALUES (''3'', ''A3'', ''	\r\n底座加工制造'', ''	\r\n2021/12/31'', ''	\r\nA2'', ''	\r\nC19'');
INSERT INTO `zjx_yxpp` VALUES (''4'', ''B1'', ''中间件原材料采购'', ''	\r\n2021/12/24'', ''	\r\nP1'', ''C8'');
INSERT INTO `zjx_yxpp` VALUES (''5'', ''B2'', ''	\r\n中间件加工制造'', ''	\r\n2021/12/27'', ''B1'', ''	\r\nC11'');
INSERT INTO `zjx_yxpp` VALUES (''6'', ''C1'', ''	\r\n外壳设计'', ''	\r\n2021/12/25'', ''	\r\nP1'', ''	\r\nC17'');
INSERT INTO `zjx_yxpp` VALUES (''7'', ''C2'', ''	\r\n外壳原材料采购'', ''	\r\n2021/12/26'', ''	\r\nC1'', ''	\r\nC1'');
INSERT INTO `zjx_yxpp` VALUES (''8'', ''C3'', ''	\r\n外壳制造'', ''2021/12/28'', ''	\r\nC2'', ''	\r\nC13'');
INSERT INTO `zjx_yxpp` VALUES (''9'', ''P2'', ''	\r\n支持总座装配'', ''	\r\n2022/1/10'', ''	\r\nP1'', ''	\r\nC20'');
INSERT INTO `zjx_yxpp` VALUES (''10'', ''P3'', ''	\r\n支持总座物流运输'', ''	\r\n2022/1/14'', ''	\r\nP2'', ''	\r\nC16'');
