package cn.scu.ne04jdemo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
/*
全局配置类-跨域请求
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    /*
    4.允许携带
     */

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("Http://localhost:8080","null")
                .allowedMethods("GET","POST","PUT")
                .allowCredentials(true)
                .maxAge(3600);

    }

}
