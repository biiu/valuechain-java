package cn.scu.ne04jdemo.controller;

import cn.scu.ne04jdemo.entitiy.Enterprise;
import cn.scu.ne04jdemo.entitiy.ST1Info;
import cn.scu.ne04jdemo.entitiy.ST2Info;
import cn.scu.ne04jdemo.entitiy.ST3Info;
import cn.scu.ne04jdemo.service.STService;
import cn.scu.ne04jdemo.vo.ST1InfoVo;
import cn.scu.ne04jdemo.vo.ST2InfoVo;
import cn.scu.ne04jdemo.vo.ST3InfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/st")

public class STController {

    @Autowired
    STService stService;

    @GetMapping("/getSt1")
    public List<ST1InfoVo> getAllSt1(){
        List<ST1InfoVo> vos = new ArrayList<>();
        List<ST1Info> allSt1 = stService.getAllSt1();
        for(ST1Info info: allSt1){
            ST1InfoVo vo = new ST1InfoVo();
            vo.setId(info.getId());
            vo.setName(info.getName());
            vo.setMaxp(info.getMaxp());
            vo.setPc(info.getPc());
            vo.setFc(info.getFc());
            vo.setFlag(info.getFlag());
            if(info.getFlag() == 1){
                vo.setUsed(true);
            }else {
                vo.setUsed(false);
            }
            vos.add(vo);
        }
        return vos;
    }

    @GetMapping("/getSt2")
    public List<ST2InfoVo> getAllSt2(){
        List<ST2InfoVo> vos = new ArrayList<>();
        List<ST2Info> allSt2 = stService.getAllSt2();
        for(ST2Info info: allSt2){
            ST2InfoVo vo = new ST2InfoVo();
            vo.setId(info.getId());
            vo.setLname(info.getLname());
            vo.setLnum(info.getLnum());
            vo.setLprice(info.getLprice());
            vo.setFlag(info.getFlag());
            if(info.getFlag() == 1){
                vo.setUsed(true);
            }else {
                vo.setUsed(false);
            }
            vos.add(vo);
        }
        return vos;
    }

    @GetMapping("/getSt3")
    public List<ST3InfoVo> getAllSt3(){
        List<ST3InfoVo> vos = new ArrayList<>();
        List<ST3Info> allSt3 = stService.getAllSt3();
        for(ST3Info info: allSt3){
            ST3InfoVo vo = new ST3InfoVo();
            vo.setId(info.getId());
            vo.setSname(info.getSname());
            vo.setEname(info.getEname());
            vo.setDist(info.getDist());
            vo.setTprice(info.getTprice());
            vo.setFlag(info.getFlag());
            if(info.getFlag() == 1){
                vo.setUsed(true);
            }else {
                vo.setUsed(false);
            }
            vos.add(vo);
        }
        return vos;
    }

    @PostMapping("/addSt1")
    public String addSt1(@RequestBody List<ST1Info> st1Infos){
        for(ST1Info st1Info: st1Infos){
            stService.insertST1(st1Info);
        }
        return "ok";
    }

    @PostMapping("/addSt2")
    public String addSt2(@RequestBody List<ST2Info> st2Infos){
        for(ST2Info st2Info: st2Infos){
            stService.insertST2(st2Info);
        }
        return "ok";
    }

    @PostMapping("/addSt3")
    public String addSt3(@RequestBody List<ST3Info> st3Infos){
        for(ST3Info st3Info: st3Infos){
            stService.insertST3(st3Info);
        }
        return "ok";
    }

    @PostMapping("/updateSt1")
    public String updateSt1(@RequestBody ST1Info st1Info){
        stService.upDateSt1(st1Info);
        return "ok";
    }

    @PostMapping("/updateSt2")
    public String updateSt2(@RequestBody ST2Info st2Info){
        stService.upDateSt2(st2Info);
        return "ok";
    }

    @PostMapping("/updateSt3")
    public String updateSt3(@RequestBody ST3Info st3Info){
        stService.upDateSt3(st3Info);
        return "ok";
    }

    @GetMapping("/deleteSt1a/{id}")
    public String deleteSt1(@PathVariable("id") Integer id){
        stService.deleteSt1(id);
        return "ok";
    }

    @GetMapping("/deleteSt2a/{id}")
    public String deleteSt2(@PathVariable("id") Integer id){
        stService.deleteSt2(id);
        return "ok";
    }

    @GetMapping("/deleteSt3a/{id}")
    public String deleteSt3(@PathVariable("id") Integer id){
        stService.deleteSt3(id);
        return "ok";
    }





}
