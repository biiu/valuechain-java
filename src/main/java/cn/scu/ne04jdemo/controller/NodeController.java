package cn.scu.ne04jdemo.controller;

import cn.scu.ne04jdemo.entitiy.Link;
import cn.scu.ne04jdemo.entitiy.Node;
import cn.scu.ne04jdemo.entitiy.NodeRelation;
import cn.scu.ne04jdemo.entitiy.User;
import cn.scu.ne04jdemo.service.NodeService;
import cn.scu.ne04jdemo.vo.MapVo;
import cn.scu.ne04jdemo.vo.NodeVo;
import cn.scu.ne04jdemo.vo.RNameVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/node")
public class NodeController {

    @Autowired
    NodeService nodeService;

    /*@PostMapping("/login")
    public String login(){
        return "ok";
    }*/

    @GetMapping("/{name}")
    public NodeVo findNodeByName(@PathVariable("name") String name){
        NodeVo nodeVo = new NodeVo();
        Node node = nodeService.findNodeByName(name);
        nodeVo.setId(node.getId());
        nodeVo.setName(node.getName());
        nodeVo.setType(node.getType());
        nodeVo.setGroup(3);

        /*nodeVo.setId(1101L);
        nodeVo.setName("11");
        nodeVo.setType("wu");
        nodeVo.setGroup(5);*/
        return nodeVo;
    }

    /*@GetMapping("/map/{name}")
    public MapVo findMapByName(@PathVariable("name") String name){
        MapVo mapVo = new MapVo();
        List<NodeVo> nodeVos = new ArrayList<>();
        //通过名称查询所有的与该节点相关的节点
        List<Node> nodes = nodeService.findAllRelationNodesByName(name);
        //通过名称查询所有的与该节点相关的关系
        List<NodeRelation> links = nodeService.findAllRelationByName(name);
        for(Node node: nodes){
            NodeVo nv = new NodeVo();
            nv.setName(node.getName());
            nv.setType(node.getType());
            nv.setGroup(5);
            nodeVos.add(nv);
        }
        mapVo.setNodes(nodeVos);
        mapVo.setLinks(links);
        return mapVo;

    }*/

    @GetMapping("/map/{name}")
    public List<NodeVo> findMapByName(@PathVariable("name") String name){
        List<NodeVo> nodeVos = new ArrayList<>();
        //通过名称查询所有的与该节点相关的节点
        List<Node> nodes = nodeService.findAllRelationNodesByName(name);
        nodes.add(nodeService.findNodeByName(name));
        for(Node node: nodes){
            NodeVo nv = new NodeVo();
            nv.setName(node.getName());
            nv.setType(node.getType());
            nv.setGroup(3);
            nodeVos.add(nv);
        }

        return nodeVos;
    }


    @GetMapping("/all")
    public List<Node> findAllNode(){
        return nodeService.findAllNode();
    }

    @GetMapping("/allLink")
    public List<NodeRelation> findAllRelation(){
        return nodeService.findAllRelation();
    }

    @GetMapping("/allLinksName")
    public List<Link> findAllRelationName(){
        List<NodeRelation> relations = nodeService.findAllRelation();
        List<Link> links = new ArrayList<>();
        for(NodeRelation relation: relations){
            links.add(new Link(relation.getSource().getName(), relation.getTarget().getName(),1));
        }

        return links;
    }

    @PostMapping("/register")
    public String login(@RequestBody Node node){
        System.out.println("===========");
        System.out.println(node);
        try {
            nodeService.createNode(node);
        }catch (Exception e){
            System.out.println(e);
            return "error";
        }
        return "ok";

    }

    @PostMapping("/relation")
    public String relation(@RequestBody RNameVo rNameVo){
        System.out.println("===========");
        System.out.println(rNameVo);
        try {
            nodeService.createRelation(rNameVo);
        }catch (Exception e){
            System.out.println(e);
            return "error";
        }
        return "ok";

    }





}
