package cn.scu.ne04jdemo.controller;

import cn.scu.ne04jdemo.entitiy.User;
import cn.scu.ne04jdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/login")
    public String login(@RequestBody User user){
        System.out.println("===========");
        System.out.println(user);
        User userByName = userService.getUserByName(user.getUser());
        /*System.out.println(userByName.getPassword());
        System.out.println(user.getPassword());*/
        if(userByName == null){
            return "error";
        }
        if(user.getPassword().equals(userByName.getPassword()) ){
            return "ok";
        }else {
            return "error";
        }
    }
}
