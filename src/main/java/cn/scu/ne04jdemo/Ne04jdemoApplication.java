package cn.scu.ne04jdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ne04jdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ne04jdemoApplication.class, args);
    }

}
