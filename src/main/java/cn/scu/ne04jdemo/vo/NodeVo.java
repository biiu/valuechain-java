package cn.scu.ne04jdemo.vo;

import cn.scu.ne04jdemo.entitiy.Node;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NodeVo extends Node {

    private Integer group;
}
