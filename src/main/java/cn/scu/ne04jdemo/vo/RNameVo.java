package cn.scu.ne04jdemo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RNameVo {
    private String before;
    private String after;
    private String text;
}
