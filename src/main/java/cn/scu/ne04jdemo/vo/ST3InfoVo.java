package cn.scu.ne04jdemo.vo;

import cn.scu.ne04jdemo.entitiy.ST3Info;
import lombok.Data;

@Data
public class ST3InfoVo extends ST3Info {
    private boolean isUsed;
}
