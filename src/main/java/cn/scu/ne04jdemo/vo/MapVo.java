package cn.scu.ne04jdemo.vo;

import cn.scu.ne04jdemo.entitiy.NodeRelation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MapVo {
    private List<NodeVo> nodes;
    private List<NodeRelation> links;
}
