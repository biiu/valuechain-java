package cn.scu.ne04jdemo.vo;

import cn.scu.ne04jdemo.entitiy.ST1Info;
import lombok.Data;

@Data
public class ST1InfoVo extends ST1Info {
    private boolean isUsed;
}
