package cn.scu.ne04jdemo.vo;

import cn.scu.ne04jdemo.entitiy.ST2Info;
import lombok.Data;

@Data
public class ST2InfoVo extends ST2Info {
    private boolean isUsed;
}
