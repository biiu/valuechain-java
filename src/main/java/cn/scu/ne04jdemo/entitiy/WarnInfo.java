package cn.scu.ne04jdemo.entitiy;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


@Data
public class WarnInfo {
    private Integer id;
    private String name;
    private String type;
    private String info;
    private String scope;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date sdate;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date edate;

}
