package cn.scu.ne04jdemo.entitiy;


import lombok.Data;

@Data
public class ST3Info {
    private Integer id;
    private String sname;
    private String ename;
    private double tprice;
    private Integer dist;
    private Integer flag;
}
