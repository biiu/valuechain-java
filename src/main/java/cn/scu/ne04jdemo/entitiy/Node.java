package cn.scu.ne04jdemo.entitiy;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@NodeEntity(label = "node")
public class Node implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    @Property
    private String name;
    @Property
    private String type;

}
