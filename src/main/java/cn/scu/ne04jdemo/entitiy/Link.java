package cn.scu.ne04jdemo.entitiy;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Link implements Serializable {
    private String source;

    private String target;

    private Integer value;

}
