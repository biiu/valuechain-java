package cn.scu.ne04jdemo.entitiy;

import lombok.Data;

@Data
public class ST1Info {
    private Integer id;
    private String name;
    private Integer maxp;
    private Integer pc;
    private Integer fc;
    private Integer flag;
}
