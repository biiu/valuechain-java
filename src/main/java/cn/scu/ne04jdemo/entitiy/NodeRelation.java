package cn.scu.ne04jdemo.entitiy;

import lombok.Data;
import org.neo4j.ogm.annotation.*;

@Data
@RelationshipEntity(type = "协同")
public class NodeRelation {
    @Id
    @GeneratedValue
    private Long id;
    @StartNode
    private Node source;
    @EndNode
    private Node target;
    @Property
    private String relation;
}
