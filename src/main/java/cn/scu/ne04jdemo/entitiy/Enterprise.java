package cn.scu.ne04jdemo.entitiy;


import lombok.Data;

@Data
public class Enterprise {
    private Integer id;
    private String name;
    private String sname;
    private String phone;
    private String des;
    private String address;
    private String tag;

}
