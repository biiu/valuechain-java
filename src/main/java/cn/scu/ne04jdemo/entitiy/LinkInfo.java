package cn.scu.ne04jdemo.entitiy;

import lombok.Data;

@Data
public class LinkInfo {
    private Integer id;
    private String name;
    private String des;
}
