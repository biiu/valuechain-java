package cn.scu.ne04jdemo.entitiy;


import lombok.Data;

@Data
public class Job {
    private Integer id;
    private String name;
    private String pname;
    private String ename;
    private String lname;
    private String des;
    private Integer jindu;
    private String date;
}
