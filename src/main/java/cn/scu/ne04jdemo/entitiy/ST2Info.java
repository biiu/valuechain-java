package cn.scu.ne04jdemo.entitiy;


import lombok.Data;

@Data
public class ST2Info {
    private Integer id;
    private String lname;
    private String lnum;
    private String lprice;
    private Integer flag;
}
