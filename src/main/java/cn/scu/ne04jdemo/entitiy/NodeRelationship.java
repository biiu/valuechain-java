package cn.scu.ne04jdemo.entitiy;

import lombok.Data;
import org.neo4j.ogm.annotation.*;

@Data
@RelationshipEntity(type = "纵向协同")
public class NodeRelationship {
    @Id
    @GeneratedValue
    private Long id;
    @StartNode
    private Node parent;
    @EndNode
    private Node child;
    @Property
    private String relation;
}
