package cn.scu.ne04jdemo.dao;

import cn.scu.ne04jdemo.entitiy.Job;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface JDao {
    @Select("select * from sys_j")
    List<Job> getAllJob();

    @Select("select * from sys_j t where t.name=#{jName}")
    Job getJobByName(String jName);

    void updateJobByName(Job job);

    int insertJ(Job job);

    @Delete("delete from sys_j where id = #{id}")
    void deleteEById(Integer id);
}
