package cn.scu.ne04jdemo.dao;

import cn.scu.ne04jdemo.entitiy.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserDao {

    @Select("select * from sys_user t where t.name=#{username}")
    User getUser(String userName);
}
