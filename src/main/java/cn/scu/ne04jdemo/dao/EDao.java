package cn.scu.ne04jdemo.dao;


import cn.scu.ne04jdemo.entitiy.*;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface EDao {

    @Select("select * from sys_e")
    List<Enterprise>  getAllEnterprise();

    @Select("select * from sys_e t where t.name=#{eName}")
    Enterprise getEnterpriseByName(String eName);

    void updateEnterpriseByName(Enterprise enterprise);

    int insertE(Enterprise enterprise);

    @Select("select * from sys_l")
    List<LinkInfo> getAllL();

    @Delete("delete from sys_e where id = #{id}")
    void deleteEById(Integer id);

    void addWarnInfo(WarnInfo warnInfo);

    @Select("select * from sys_warn")
    List<WarnInfo> getAllW();

    @Delete("delete from sys_warn where name = #{name}")
    void deleteWarn(String name);

    @Select("select * from zjx_lldy")
    List<Nldy> getNldy();

    @Delete("delete from zjx_lldy where num = #{num}")
    void deleteNldy(String num);

    void updateNldy(Nldy nldy);

    @Select("select * from zjx_pjjg")
    List<Pjjg> getPjjg();

    @Select("select * from zjx_yxpp")
    List<Yxpp> getYxpp();

    @Select("select * from zjx_qypjmb")
    List<Qypjmb> getQypjmb();

    @Select("select * from zjx_qypj")
    List<Qypj> getQypj();

    @Select("select * from zjx_jg")
    List<Jg> getJg();
}
