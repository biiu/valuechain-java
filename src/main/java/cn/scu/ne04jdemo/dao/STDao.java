package cn.scu.ne04jdemo.dao;

import cn.scu.ne04jdemo.entitiy.ST1Info;
import cn.scu.ne04jdemo.entitiy.ST2Info;
import cn.scu.ne04jdemo.entitiy.ST3Info;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface STDao {

    @Select("select * from sys_st1")
    List<ST1Info> getAllSt1();

    @Select("select * from sys_st2")
    List<ST2Info> getAllSt2();

    @Select("select * from sys_st3")
    List<ST3Info> getAllSt3();

    @Insert("insert into sys_st1(name,maxp,pc,fc) values (#{name},#{maxp},#{pc},#{fc})")
    void insertST1(ST1Info st1Info);

    @Insert("insert into sys_st2(lname,lnum,lprice) values (#{lname},#{lnum},#{lprice})")
    void insertST2(ST2Info st2Info);

    @Insert("insert into sys_st3(sname,ename,tprice,dist) values (#{sname},#{ename},#{tprice},#{dist})")
    void insertST3(ST3Info st3Info);

    void upDateSt1(ST1Info st1Info);

    void upDateSt2(ST2Info st2Info);

    void upDateSt3(ST3Info st3Info);

    @Delete("delete from sys_st1 where id = #{id}")
    void deleteSt1(Integer id);

    @Delete("delete from sys_st2 where id = #{id}")
    void deleteSt2(Integer id);

    @Delete("delete from sys_st3 where id = #{id}")
    void deleteSt3(Integer id);

}
