package cn.scu.ne04jdemo.dao;

import cn.scu.ne04jdemo.entitiy.NodeRelation;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface NodeRelationRepository extends Neo4jRepository<NodeRelation, Long> {
}
