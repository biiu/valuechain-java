package cn.scu.ne04jdemo.dao;

import cn.scu.ne04jdemo.entitiy.Node;
import cn.scu.ne04jdemo.entitiy.NodeRelation;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VRepository extends Neo4jRepository <Node, Long> {

    @Query("match (n:node {name: {0}}), (m:node {name: {2}}) " +
            "create (n)-[:协同{relation: {1}}]->(m)")
    void createRelation(String from, String relation, String to);

    @Query("match (n:node)-[rel]-(m:node) where id(rel)={0} delete rel")
    void deleteRelationById(Long id);

    @Query("match (n: node {name: {0}}) return n")
    Node findNodeByName(String name);

    @Query("MATCH p=()-->() RETURN p")
    Iterable<NodeRelation> findAllRelation();

    @Query("MATCH (n:node)-[r]->(b:node) where n.name={0} return b ")
    List<Node> findTargetByname(String nodeName);

    @Query("MATCH (n:node)-[r]->(b:node) where b.name={0} return n ")
    List<Node> findSourceByname(String nodeName);

    @Query("MATCH (n:node)-[r]->(b:node) where b.name={0} return n,r,b ")
    Iterable<NodeRelation> findBeforeByname(String nodeName);

    @Query("MATCH (n:node)-[r]->(b:node) where n.name={0} return n,r,b ")
    Iterable<NodeRelation> findAfterByname(String nodeName);





}
