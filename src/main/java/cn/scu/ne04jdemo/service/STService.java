package cn.scu.ne04jdemo.service;

import cn.scu.ne04jdemo.entitiy.ST1Info;
import cn.scu.ne04jdemo.entitiy.ST2Info;
import cn.scu.ne04jdemo.entitiy.ST3Info;
import org.apache.ibatis.annotations.Delete;

import java.util.List;

public interface STService {

    List<ST1Info> getAllSt1();

    List<ST2Info> getAllSt2();

    List<ST3Info> getAllSt3();

    void insertST1(ST1Info st1Info);

    void insertST2(ST2Info st2Info);

    void insertST3(ST3Info st3Info);

    void upDateSt1(ST1Info st1Info);

    void upDateSt2(ST2Info st2Info);

    void upDateSt3(ST3Info st3Info);

    void deleteSt1(Integer id);

    void deleteSt2(Integer id);

    void deleteSt3(Integer id);
}
