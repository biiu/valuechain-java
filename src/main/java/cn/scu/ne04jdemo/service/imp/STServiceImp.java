package cn.scu.ne04jdemo.service.imp;

import cn.scu.ne04jdemo.dao.STDao;
import cn.scu.ne04jdemo.entitiy.ST1Info;
import cn.scu.ne04jdemo.entitiy.ST2Info;
import cn.scu.ne04jdemo.entitiy.ST3Info;
import cn.scu.ne04jdemo.service.STService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class STServiceImp implements STService {

    @Autowired
    STDao stDao;

    @Override
    public List<ST1Info> getAllSt1() {
        return stDao.getAllSt1();
    }

    @Override
    public List<ST2Info> getAllSt2() {
        return stDao.getAllSt2();
    }

    @Override
    public List<ST3Info> getAllSt3() {
        return stDao.getAllSt3();
    }

    @Override
    public void insertST1(ST1Info st1Info) {
        stDao.insertST1(st1Info);
    }

    @Override
    public void insertST2(ST2Info st2Info) {
        stDao.insertST2(st2Info);
    }

    @Override
    public void insertST3(ST3Info st3Info) {
        stDao.insertST3(st3Info);

    }

    @Override
    public void upDateSt1(ST1Info st1Info) {
        stDao.upDateSt1(st1Info);
    }

    @Override
    public void upDateSt2(ST2Info st2Info) {
        stDao.upDateSt2(st2Info);
    }

    @Override
    public void upDateSt3(ST3Info st3Info) {
        stDao.upDateSt3(st3Info);
    }

    @Override
    public void deleteSt1(Integer id) {
        stDao.deleteSt1(id);
    }

    @Override
    public void deleteSt2(Integer id) {
        stDao.deleteSt2(id);
    }

    @Override
    public void deleteSt3(Integer id) {
        stDao.deleteSt3(id);
    }


}
