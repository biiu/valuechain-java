package cn.scu.ne04jdemo.service.imp;

import cn.scu.ne04jdemo.dao.EDao;
import cn.scu.ne04jdemo.dao.JDao;
import cn.scu.ne04jdemo.entitiy.*;
import cn.scu.ne04jdemo.service.EService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EServiceImp implements EService {

    @Autowired
    EDao eDao;

    @Autowired
    JDao jDao;

    @Override
    public List<Enterprise> getAllE() {
        return eDao.getAllEnterprise();
    }

    @Override
    public Enterprise getEByName(String eName) {
        return eDao.getEnterpriseByName(eName);
    }

    @Override
    public void updateE(Enterprise enterprise)   {
        try {
            eDao.updateEnterpriseByName(enterprise);
        }catch (Exception e){
            throw e;
        }
    }

    @Override
    public int insertE(Enterprise enterprise) {
        return eDao.insertE(enterprise);
    }

    @Override
    public List<Job> getAllJ() {
        return jDao.getAllJob();
    }

    @Override
    public Job getJByName(String jName) {
        return jDao.getJobByName(jName);
    }

    @Override
    public void updateJ(Job job) {
        try {
            jDao.updateJobByName(job);
        }catch (Exception e){
            throw e;
        }
    }

    @Override
    public int insertJ(Job job) {
        return jDao.insertJ(job);
    }

    @Override
    public List<LinkInfo> getAllL() {
        return eDao.getAllL();
    }

    @Override
    public void deleteE(Integer id) {
        eDao.deleteEById(id);
    }

    @Override
    public void deleteJ(Integer id) {
        jDao.deleteEById(id);
    }

    @Override
    public void addWarnInfo(WarnInfo warnInfo) {
        eDao.addWarnInfo(warnInfo);
    }

    @Override
    public List<WarnInfo> getAllW() {
        return eDao.getAllW();
    }

    @Override
    public void deleteWarn(String name) {
        eDao.deleteWarn(name);
    }

    @Override
    public List<Nldy> getNldy() {
        return eDao.getNldy();
    }

    @Override
    public void updateNldy(Nldy nldy) {
        eDao.updateNldy(nldy);
    }

    @Override
    public void deleteNldy(String num) {
        eDao.deleteNldy(num);
    }

    @Override
    public List<Pjjg> getPjjg() {
        return eDao.getPjjg();
    }

    @Override
    public List<Yxpp> getYxpp() {
        return eDao.getYxpp();
    }

    @Override
    public List<Qypjmb> getQypjmb() {
        return eDao.getQypjmb();
    }

    @Override
    public List<Qypj> getQypj() {
        return eDao.getQypj();
    }

    @Override
    public List<Jg> getJg() {
        return eDao.getJg();
    }


}
