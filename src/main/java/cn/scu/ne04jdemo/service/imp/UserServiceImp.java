package cn.scu.ne04jdemo.service.imp;

import cn.scu.ne04jdemo.dao.UserDao;
import cn.scu.ne04jdemo.entitiy.User;
import cn.scu.ne04jdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImp implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public User getUserByName(String userName) {
        return userDao.getUser(userName);
    }
}
