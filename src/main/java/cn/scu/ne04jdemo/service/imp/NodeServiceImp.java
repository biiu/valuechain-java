package cn.scu.ne04jdemo.service.imp;

import cn.scu.ne04jdemo.dao.VRepository;
import cn.scu.ne04jdemo.entitiy.Node;
import cn.scu.ne04jdemo.entitiy.NodeRelation;
import cn.scu.ne04jdemo.service.NodeService;
import cn.scu.ne04jdemo.vo.RNameVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class NodeServiceImp implements NodeService {

    @Autowired
    VRepository repository;

    @Override
    public Node findNodeById(Long id) {
        Optional<Node> byId = repository.findById(id);
        return byId.get();
    }

    @Override
    public Node findNodeByName(String name) {
        return repository.findNodeByName(name);
    }

    @Override
    public List<Node> findAllNode() {
        List<Node> resNodes = new ArrayList<>();
        Iterable<Node> nodes = repository.findAll();
        for(Node node: nodes){
            resNodes.add(node);
        }
        return resNodes;
    }

    @Override
    public List<NodeRelation> findAllRelation() {
        List<NodeRelation> resList = new ArrayList<>();
        Iterable<NodeRelation> relations = repository.findAllRelation();
        for(NodeRelation nr: relations){
            resList.add(nr);
        }
        return resList;
    }
    /*
    查找与该节点有关的所有节点
     */

    @Override
    public List<Node> findAllRelationNodesByName(String nodeName) {
        Set<Node> targetNodesets = new HashSet<>();
        Set<Node> sourceNodesets = new HashSet<>();
        List<Node> res = new ArrayList<>();
        //查找后驱节点
        List<Node> targetNode = repository.findTargetByname(nodeName);
        Queue<Node> queue = new LinkedList<>();
        for(Node n: targetNode){
            queue.add(n);
            targetNodesets.add(n);
        }
        while(queue.size() > 0){
            int len = queue.size();
            for(int i = 0; i < queue.size(); i++){
                List<Node> temp = repository.findTargetByname(queue.poll().getName());
                targetNodesets.addAll(temp);
                queue.addAll(temp);
            }
        }

        //查找前驱节点
        List<Node> sourceNode = repository.findSourceByname(nodeName);
        Queue<Node> sourcequeue = new LinkedList<>();
        for(Node n: sourceNode){
            sourcequeue.add(n);
            sourceNodesets.add(n);
        }
        while(sourcequeue.size() > 0){
            int len = sourcequeue.size();
            for(int i = 0; i < len; i++){
                List<Node> temp = repository.findSourceByname(sourcequeue.poll().getName());
                sourceNodesets.addAll(temp);
                sourcequeue.addAll(temp);
            }
        }

        res.addAll(targetNodesets);
        res.addAll(sourceNodesets);
        return res;
    }

    @Override
    public List<NodeRelation> findAllRelationByName(String nodeName) {
        return null;
    }

    @Override
    public void createNode(Node node) {
        repository.save(node);
    }

    @Override
    public void createRelation(RNameVo rNameVo) {
        repository.createRelation(rNameVo.getBefore(),rNameVo.getText(),rNameVo.getAfter());
    }
}
