package cn.scu.ne04jdemo.service;

import cn.scu.ne04jdemo.entitiy.*;

import java.util.List;

public interface EService {
    List<Enterprise> getAllE();

    Enterprise getEByName(String eName);

    void updateE(Enterprise enterprise);

    int insertE(Enterprise enterprise);

    List<Job> getAllJ();

    Job getJByName(String jName);

    void updateJ(Job job);

    int insertJ(Job job);

    List<LinkInfo> getAllL();

    void deleteE(Integer id);

    void deleteJ(Integer id);

    void addWarnInfo(WarnInfo warnInfo);

    List<WarnInfo> getAllW();

    void deleteWarn(String name);

    List<Nldy> getNldy();

    void updateNldy(Nldy nldy);

    void deleteNldy(String num);

    List<Pjjg> getPjjg();

    List<Yxpp> getYxpp();

    List<Qypjmb> getQypjmb();

    List<Qypj> getQypj();

    List<Jg> getJg();
}
