package cn.scu.ne04jdemo.service;

import cn.scu.ne04jdemo.entitiy.User;

public interface UserService {

    User getUserByName(String userName);
}
