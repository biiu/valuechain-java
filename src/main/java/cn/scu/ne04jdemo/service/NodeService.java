package cn.scu.ne04jdemo.service;

import cn.scu.ne04jdemo.entitiy.Node;
import cn.scu.ne04jdemo.entitiy.NodeRelation;
import cn.scu.ne04jdemo.vo.RNameVo;

import java.util.List;

public interface NodeService {
    Node findNodeById(Long id);

    Node findNodeByName(String name);

    List<Node> findAllNode();

    List<NodeRelation> findAllRelation();

    List<Node> findAllRelationNodesByName(String nodeName);

    List<NodeRelation> findAllRelationByName(String nodeName);

    void createNode(Node node);

    void createRelation(RNameVo rNameVo);
}
