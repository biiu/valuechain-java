package cn.scu.ne04jdemo;

import cn.scu.ne04jdemo.dao.VRepository;
import cn.scu.ne04jdemo.entitiy.*;
import cn.scu.ne04jdemo.service.EService;
import cn.scu.ne04jdemo.service.NodeService;
import cn.scu.ne04jdemo.service.UserService;
import cn.scu.ne04jdemo.service.imp.NodeServiceImp;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

@SpringBootTest
class Ne04jdemoApplicationTests {

    @Autowired
    VRepository repository;
    @Autowired
    NodeServiceImp nodeServiceImp;
    @Autowired
    NodeService nodeService;
    @Autowired
    UserService userService;

    @Autowired
    EService eService;


    @Test
    public void testCreate(){
        /*Optional<Node> byId = repository.findById(44L);
        byId.orElse(null);
        System.out.println(byId.get().getName());*/

        /*Node node = new Node();
        node.setName("V2.1");
        node.setType("加工制造");
        repository.save(node);*/

        for(int i = 1; i <= 1; i++){
            Node node = new Node();
            node.setName("V9." + i);
            node.setType("售后");
            repository.save(node);
        }
    }



    @Test
    public void testCreateRelation(){
        repository.createRelation("V7.1","","V8.1");
        repository.createRelation("V7.1","","V8.2");
        repository.createRelation("V7.2","","V8.3");
        repository.createRelation("V7.3","","V8.4");
        repository.createRelation("V7.3","","V8.5");
        repository.createRelation("V7.4","","V8.6");
        repository.createRelation("V7.5","","V8.7");
        repository.createRelation("V7.5","","V8.8");

    }

    @Test
    public void testDeleteRelationById(){
        repository.deleteRelationById(21L);
    }


    @Test
    public void findById(){
        Node nodeById = nodeServiceImp.findNodeById(47L);
        System.out.println(nodeById.getName());
    }

    @Test
    public void findByName(){
        Node nodeById = nodeServiceImp.findNodeByName("V3.1");
        System.out.println(nodeById);
    }

    @Test
    public void queryAllRelation(){
        List<NodeRelation> relation = nodeService.findAllRelation();
        relation.stream().forEach((v)-> System.out.println(v));
    }

    @Test
    public void queryAll(){
        List<Node> allNode = nodeService.findAllNode();
        for(Node node: allNode){
            System.out.println(node);
        }
    }

    @Test
    public void queryTargetByNodeName(){
        List<Node> nodes = repository.findTargetByname("V1.1");
        for(Node node: nodes){
            System.out.println(node.getName());
        }
    }

    @Test
    public void querySourceByNodeName(){
        List<Node> nodes = repository.findSourceByname("V2.1");
        for(Node node: nodes){
            System.out.println(node.getName());
        }
    }

    @Test
    public void queryBeforeByNodeName(){
        Iterable<NodeRelation> beforeByname = repository.findAfterByname("V3.2");
        //System.out.println(beforeByna);
        /*System.out.println(links.size());
        for(NodeRelation link: links){
            System.out.println(link.getSource() + "->" + link.getTarget());
        }*/
        beforeByname.forEach(v -> System.out.println(v.getSource().getName()));
        for(NodeRelation relation: beforeByname){
            System.out.println(relation.getSource().getName() + "->" + relation.getTarget().getName());
        }
    }

    @Test
    public void queryTargete(){
        List<Node> nodes = nodeService.findAllRelationNodesByName("V8.1");
        for(Node node: nodes){
            System.out.println(node.getName());
        }
        System.out.println(nodes.size());
    }

    @Test
    public void querUser(){
        User user = userService.getUserByName("admin");
        System.out.println(userService.getUserByName("admin").getPassword());
        System.out.println(user);
    }

    @Test
    public void deleteById(){
        repository.deleteById(93L);
    }

    @Test
    public void addEJ(){
        Enterprise e = new Enterprise();
        Job job = new Job();
        e.setName("京东方科技集团股份有限公司");
        e.setSname("京东方");
        e.setPhone("11111");
        e.setAddress("中国北京市经济技术开发区西环中路12号");
        e.setDes("为信息交互和人类健康提供智慧端口产品和专业服务的物联网公司");
        e.setTag("核心");
        //eService.updateE(e);

        job.setName("t1");
        job.setDes("test");
        job.setDate("2021-1-2");
        //eService.insertJ(job);
        //eService.updateJ(job);

        Job j = eService.getJByName("空调外机");
        System.out.println(j.getDes());
        Enterprise e1 = eService.getEByName("华为");
        System.out.println(e1.getAddress());

    }

    @Test
    public void getAllL(){
        List<LinkInfo> allL = eService.getAllL();
        for(LinkInfo l: allL){
            System.out.println(l.getName());
        }
    }

    @Test
    public void getAllW(){
        List<WarnInfo> allW = eService.getAllW();
        for(WarnInfo warnInfo: allW){
            System.out.println(warnInfo);
        }
    }

    @Test
    public void getNldy(){
        List<Nldy> nldy = eService.getNldy();
        nldy.stream().forEach(e-> System.out.println(e));
    }


}
